# Awesome Gitlab

A curated list of amazingly awesome gitlab and gitlab-ci resources

## Official Documentation

- [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)
- [GitLab CI/CD Pipeline Configuration Reference](https://docs.gitlab.com/ee/ci/yaml/)
- [GitLab CI/CD environment variables](https://docs.gitlab.com/ee/ci/variables/)
- [Official Collection of .gitlab-ci.yml templates](https://gitlab.com/gitlab-org/gitlab-foss/tree/master/lib/gitlab/ci/templates)

## Gitlab API libraries and tools

- [python-gitlab](https://github.com/python-gitlab/python-gitlab) Python package providing access to the GitLab server API and CLI tool.
- [gitlab ruby](https://github.com/NARKOZ/gitlab/) Ruby wrapper and CLI for the GitLab REST API.
- [snippet](https://gitlab.com/zj/snippet) CLI tool to pipe to a GitLab Snippet.
- [slack-unfurl-gitlab](https://github.com/glensc/slack-unfurl-gitlab) GitLab links unfurler for slack-unfurl.
- [gitlab-unfurly](https://github.com/kiwicom/gitlab-unfurly) Serverless Slack bot for unfurling GitLab URLs.

## Gitlab CI Tools

- [gitlab-ci-helpers](https://gitlab.com/morph027/gitlab-ci-helpers) Scripts to make CI usage easier: `get-last-successful-build-artifact`, `keep-tagged-artifacts` and `swarm`-related script. Also, check [wiki](https://gitlab.com/morph027/gitlab-ci-helpers/wikis/home).
- [pipeline-trigger](https://gitlab.com/finestructure/pipeline-trigger) Pipeline-trigger allows you to trigger and wait for the results of another GitLab pipeline.
- [gitlab-ci-pipeline-queue](https://gist.github.com/metanovii/afdb6a98844feff210b591a4bc249771) Trick to force waiting for previous pipeline finished.
- [gitlab-ci-monorepo](https://github.com/BastiPaeltz/gitlab-ci-monorepo) Walkthrough and examples on how GitLab 10.7+ and 11.0+ help managing CI when using monorepos.
- [gitlab-registry-cleaner](https://github.com/tnextday/gitlab-registry-cleaner) Clean up gitlab registry using the Gitlab API. 

## Other Awesome Lists

Other amazingly awesome lists can be found in the [awesome-awesomeness](https://github.com/bayandin/awesome-awesomeness) list.

## Contributing

Your contributions are always welcome!
